package com.mc4.protostore.data;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;

import com.google.common.base.Predicate;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static com.google.common.base.Strings.isNullOrEmpty;

/**
 * Created by miguel on 7/2/13.
 */
public class DataStore {
    final static private Iterable<Item> dataSource;

    static {
        final List<Item> items = new ArrayList<Item>();
        items.add(new Item("Samsung Galaxy Tab 2 (7-Inch, Wi-Fi)", "", "Samsung", "$169.00", bitmap("tablet_0.jpg")));
        items.add(new Item("Samsung Galaxy Tab 2 (10.1-Inch, Wi-Fi)", "", "Samsung", "$299.00", bitmap("tablet_1.jpg")));
        items.add(new Item("7\" Tablet Stand with USB Keyboard - Black Faux Leather Carrying Case ", "", "EB", "$6.91", bitmap("tablet_2.jpg")));
        items.add(new Item(" Chromo Inc® White 4gb 7\" Android 4.2 Jellybean Touch Capacitive Screen 1.2ghz 512 RAM Tablet Pc Wifi 3g Camera Tr-a13", "", "Chromo Inc", "$169.00", bitmap("tablet_3.jpg")));
        items.add(new Item("Blackberry Playbook 7-Inch Tablet (16GB)", "", "BlackBerry", "$154.25", bitmap("tablet_4.jpg")));
        items.add(new Item("Samsung Chromebook (Wi-Fi, 11.6-Inch)", "", "Samsung", "$249.00", bitmap("tablet_5.jpg")));
        items.add(new Item("7\" A13 Google Android 4.0 AllWinner Tablet Boxchip Cortex A8 1.2Ghz MID Capacitive Touch Screen G-sensor WIFI", "", "TabletExpress", "$64.99", bitmap("tablet_6.jpg")));
        items.add(new Item("9\" Google Android 4.0 8GB MID Capacitive Touch Screen G-sensor A13 Tablet MID948B - Dragon Touch (TM)", "", "TableExpress", "$96.99", bitmap("tablet_7.jpg")));
        items.add(new Item("Alldaymall(TM) 7-inch Capacitive Touch Screen Android 4.0 Tablet PC with Allwinner A13 1.0GHz 512MB/4GB WiFi Front-camera", "", "Alldaymall", "$64.99", bitmap("tablet_8.jpg")));
        items.add(new Item("Coby Kyros 7-Inch Android 4.0 4 GB Internet Tablet 16:9 Resistive Touchscreen - MID7046-4 (Black)", "", "Coby", "$66.99", bitmap("tablet_9.jpg")));
        items.add(new Item("Coby Kyros 7-Inch Android 4.0 4 GB Internet Tablet 16:9 Capacitive Multi-Touch Widescreen with Built-In Camera", "", "Coby", "$100.94", bitmap("tablet_10.jpg")));
        items.add(new Item("Coby Kyros 7-Inch Android 4.0 4 GB Internet Tablet 16:9 Resistive Touchscreen, Black MID7034-4", "", "Coby", "$69.99", bitmap("tablet_11.jpg")));

        items.add(new Item("Samsung Chromebook (Wi-Fi, 11.6-Inch)", "",
                "Samsung", "$399.99", bitmap("laptop_0.jpg")));
        items.add(new Item("Dell Inspiron 15 i15RV-6190BLK 15.6-Inch Laptop (Black Matte with Textured Finish)", "",
                "Dell", "$379.99", bitmap("laptop_1.jpg")));
        items.add(new Item("Apple iBook Laptop, G4 iBook 1.33GHz Processor, 1GB, 40GB, 12.1\" 1024x768 Display, Combo Drive ,WiFi, 56K Modem", "",
                "Apple", "$238.99", bitmap("laptop_2.jpg")));
        items.add(new Item("HP Pavilion G6-2235us 15.6\" Laptop (2.7 GHz AMD A6-4400M Accelerated Processor, 4GB RAM, 750GB Hard Drive, SuperMulti", "",
                "Dell", "$385.99", bitmap("laptop_3.jpg")));
        items.add(new Item("ASUS K55N-DS81 15.6-Inch Laptop (Black)", "",
                "Asus", "$410.99", bitmap("laptop_4.jpg")));
        items.add(new Item("Acer C7 C710-2847 Chromebook 11.6\" Intel Dual Core B847 1.1 GHz 2GB DDR3 320GB 5400RPM HDD Wifi HDMI USB3.0 VGA", "",
                "Acer", "$251.99", bitmap("laptop_5.jpg")));
        items.add(new Item("ASUS A53U A53U-AS21 15.6-Inch Laptop (Mocha)", "",
                "Asus", "$338.99", bitmap("laptop_6.jpg")));
        items.add(new Item("HP 2000-2b09WM Fusion E-300 1.3GHz 2GB 320GB DVD±RW 15.6\" Windows 8 Laptop (Winter Blue)", "",
                "HP", "$351.99", bitmap("laptop_7.jpg")));
        items.add(new Item("Dell Inspiron 15 i15RM-5123SLV 15.6-Inch Laptop (Moon Silver)", "",
                "Dell", "$539.99", bitmap("laptop_8.jpg")));
        items.add(new Item("Dell D620 Laptop Duo Core with Windows XP", "",
                "Dell", "$119.99", bitmap("laptop_9.jpg")));
        items.add(new Item("Dell Inspiron i15R-1316BLU 15-Inch Laptop", "",
                "Dell", "$1324.99", bitmap("laptop_10.jpg")));
        items.add(new Item("Toshiba Satellite C855D-S5320 15.6-Inch Laptop (Satin Black Trax)", "",
                "Toshiba", "$650.99", bitmap("laptop_10.jpg")));

        dataSource = new Iterable<Item>() {
            @Override
            public Iterator<Item> iterator() {
                return items.iterator();
            }
        };
    }

    public static Iterable<Item> filter(final String keyword) {
        if (isNullOrEmpty(keyword)) {
            return new Iterable<Item>() {
                @Override
                public Iterator<Item> iterator() {
                    return Collections.<Item>emptyList().iterator();
                }
            };
        }

        return Iterables.filter(dataSource, new Predicate<Item>() {
            @Override
            public boolean apply(com.mc4.protostore.data.Item item) {
                if (item.getDesc().toLowerCase().contains(keyword.toLowerCase()) || item.getTitle().toLowerCase().contains(keyword.toLowerCase())
                        || item.getOwner().toLowerCase().contains(keyword.toLowerCase()))
                    return true;
                return false;
            }
        });
    }

    static Bitmap bitmap(String res) {
        InputStream in = DataStore.class.getResourceAsStream("/"+res);
        Bitmap bitmap = BitmapFactory.decodeStream(in);;

//        Bitmap b = BitmapFactory.decodeByteArray(imageAsBytes, 0, imageAsBytes.length);
//        profileImage.setImageBitmap(Bitmap.createScaledBitmap(b, 120, 120, false));

        return Bitmap.createScaledBitmap(bitmap, 160, 160, false);
    }
}