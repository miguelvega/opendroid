package com.mc4.protostore.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

/**
 * Created by miguel on 7/2/13.
 */
public class Item {
    private String title, desc, owner, price;

    private Drawable img;

    private Bitmap bitmap;

    public Item() {
        super();
    }

    public Item(String title, String desc, String owner, String price, Bitmap bitmap){
        this(title, desc, owner, price, (Drawable)null);
        this.bitmap = bitmap;
    }
    public Item(String title, String desc, String owner, String price, Drawable img){
        this.title = title;
        this.desc = desc;
        this.owner = owner;
        this.price = price;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public Item setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDesc() {
        return desc;
    }

    public Item setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public String getOwner() {
        return owner;
    }

    public Item setOwner(String owner) {
        this.owner = owner;
        return this;
    }

    public String getPrice() {
        return price;
    }

    public Item setPrice(String price) {
        this.price = price;
        return this;
    }

    public Drawable getDrawable() {
        return img;
    }

    public Item setDrawable(Drawable img) {
        this.img = img;
        return this;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
