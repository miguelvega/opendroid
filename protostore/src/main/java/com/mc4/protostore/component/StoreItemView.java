package com.mc4.protostore.component;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mc4.protostore.R;
import com.mc4.protostore.data.Item;

/**
 * Created by miguel on 7/2/13.
 */
public class StoreItemView extends LinearLayout {
    public StoreItemView(Context context, Item item) {
        this(context, null, item);

    }

    public StoreItemView(Context context, AttributeSet attrs, Item item) {
        super(context, attrs);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        inflater.inflate(R.layout.table_item, this);

        //
        TextView title = (TextView) findViewById(R.id.itemTitle);
        title.setText(item.getTitle());


        TextView owner = (TextView) findViewById(R.id.itemOwner);
        owner.setText(item.getOwner());

        TextView price = (TextView) findViewById(R.id.itemPrice);
        price.setText(item.getPrice());

        ImageView image = (ImageView) findViewById(R.id.itemImage);
        image.setImageDrawable(new BitmapDrawable(getResources(), item.getBitmap()));

    }
}
