package com.mc4.protostore;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.collect.Iterables;
import com.mc4.protostore.component.HorizontalNumberPicker;
import com.mc4.protostore.component.StoreItemView;
import com.mc4.protostore.data.DataStore;
import com.mc4.protostore.data.Item;

import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;

/**
 * Created by miguel on 7/1/13.
 */
public class StoreFragment extends Fragment implements View.OnClickListener{
    private TableLayout table;

    public StoreFragment() {
        super();
    }

    int dip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.store_fragment, container, false);

        this.table = (TableLayout) root.findViewById(R.id.results_table);

//        updateResults(getActivity(), new Iterable<Item>() {
//            @Override
//            public Iterator<Item> iterator() {
//                return Collections.<Item>emptyList().iterator();
//            }
//        });

        this.dip = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,
                (float) 1, getResources().getDisplayMetrics());

        updateResults(getActivity(), DataStore.filter("samsung"));

        return root;
    }

    public void updateResults(Activity activity, Iterable<Item> data){


        if(!Iterables.isEmpty(data)){
            //initialize the table header and other features

        }

        boolean headOn = false;

        table.removeAllViews();

        int numRow = 0;

        TableRow row = null;

        for (Item item: data) {

            if(!headOn){

            }

            int irow = numRow++ % 2;

            if(irow==0){
                row = new TableRow(getActivity());
                StoreItemView sv = new StoreItemView(getActivity(), item);
                sv.setOnClickListener(this);
                row.addView(sv);
            }else{
                StoreItemView sv = new StoreItemView(getActivity(), item);
                sv.setOnClickListener(this);
                row.addView(sv);

                table.addView(row, new TableLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                row = null;
            }

//
//            TextView t1 = new TextView(getActivity());
//            t1.setTextColor(getResources().getColor(R.color.yellow));
//            t1.setText(item.getTitle());
//            t1.setTypeface(null, 1);
//            t1.setTextSize(15);
//            //t1.setWidth(50 * dip);
//            t1.setPadding(20*dip, 0, 0, 0);
//            //row.addView(t1);
//            TextView t2 = new TextView(getActivity());
//            t2.setTextColor(getResources().getColor(R.color.dark_red));
//            t2.setText("Boliiva");
//            t2.setTypeface(null, 1);
//            t2.setTextSize(15);
//            t2.setWidth(150 * dip);
//            //row.addView(t2);
        }
        if(row!=null){
            table.addView(row, new TableLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        }
    }

    @Override
    public void onClick(View view) {
        Toast.makeText(getActivity(), "You've seleceted the "+view, Toast.LENGTH_SHORT);
    }
}