package com.mc4.protostore;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;

import com.google.common.collect.Iterables;
import com.mc4.protostore.component.StoreItemView;
import com.mc4.protostore.data.DataStore;
import com.mc4.protostore.data.Item;

/**
 * Created by miguel on 7/1/13.
 */
public class EntertainmentFragment extends Fragment {

    public EntertainmentFragment() {
        super();
    }

    int dip;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.entertainment, container, false);
        return root;
    }
}